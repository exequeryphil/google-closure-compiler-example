# Usage
* Install closure-library
* Compile JS with jar. (outputs to ./dist/js/main.js)
Example:
```
java -jar closure-compiler/compiler.jar --js "closure-library/closure/goog/base.js" --js "js/modules" --js "js/main.js" --js_output_file "dist/js/main.js"  --compilation_level "ADVANCED_OPTIMIZATIONS" --dependency_mode "STRICT" --entry_point "js/main.js" --language_in "ECMASCRIPT6" --language_out "ECMASCRIPT5_STRICT" --output_manifest "manifest.MNF"
```
* Available flags can be found by running help:
```
  java -jar closure-compiler/compiler.jar --help
```